package com.example.hellofolk.ui.posts


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LifecycleOwner
import androidx.test.ext.junit.runners.AndroidJUnit4


import com.example.hellofolk.TestCoroutineRule
import com.example.hellofolk.domain.usecase.post.PostsUseCases
import com.example.hellofolk.ui.post.PostsViewModel
import com.example.hellofolk.utlis.helpers.SingleLiveEvent
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class PostsViewModelTest {

    // Set the main coroutines dispatcher for unit testing.
    @get:Rule
    var mainCoroutineRule = TestCoroutineRule()

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private var lifecycleOwner: LifecycleOwner? = null

    @RelaxedMockK
    private lateinit var isLoadingLiveData: SingleLiveEvent<Boolean>

    @RelaxedMockK
    private lateinit var mockException: Exception

    @RelaxedMockK
    private lateinit var mockUseCase: PostsUseCases

    @RelaxedMockK
    private lateinit var viewModel: PostsViewModel

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        isLoadingLiveData = viewModel.progressVisible
        every { mockException.message } returns "Test Exception"
        viewModel = PostsViewModel(mockUseCase)
    }

    @Test
    fun loadingChanging() {
        viewModel.bound(true)
        lifecycleOwner?.let {
            isLoadingLiveData.observe(it, { aBoolean ->
                assertTrue(aBoolean)
            })
        }
    }

    @Test
    fun `Loading Changed On Start Bounding`() = mainCoroutineRule.run {
        // GIVEN
        val actual = true

        every { viewModel.bound(true) }

        // WHEN
        val expected = viewModel.progressVisible.value

        // THEN
        assertEquals(expected, actual)
    }

    @Test
    fun `Loading Not Changed On Start Bounding`() = mainCoroutineRule.run {
        // GIVEN
        val actual = false

        every { viewModel.bound(true) }

        // WHEN
        val expected = viewModel.progressVisible.value

        // THEN
        assertNotEquals(expected, actual)
    }

}