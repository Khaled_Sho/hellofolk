package com.example.hellofolk.ui.users


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LifecycleOwner
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.hellofolk.TestCoroutineRule
import com.example.hellofolk.domain.usecase.user.GetUsersUseCase
import com.example.hellofolk.domain.usecase.user.InsertUserUseCase
import com.example.hellofolk.users.view.user.UsersViewModel
import com.example.hellofolk.utlis.helpers.SingleLiveEvent
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class UsersViewModelTest {

    // Set the main coroutines dispatcher for unit testing.
    @get:Rule
    var mainCoroutineRule = TestCoroutineRule()

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private var lifecycleOwner: LifecycleOwner? = null

    @RelaxedMockK
    private lateinit var isLoadingLiveData: SingleLiveEvent<Boolean>

    @RelaxedMockK
    private lateinit var mockException: Exception

    @RelaxedMockK
    private lateinit var mockGetUseCase: GetUsersUseCase

    @RelaxedMockK
    private lateinit var mockInsertUseCase: InsertUserUseCase

    @RelaxedMockK
    private lateinit var viewModel: UsersViewModel

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        isLoadingLiveData = viewModel.usersLoading
        every { mockException.message } returns "Test Exception"
        viewModel = UsersViewModel(mockGetUseCase, mockInsertUseCase)
    }

    @Test
    fun loadingChanging() {
        viewModel.bound(true)
        lifecycleOwner?.let {
            isLoadingLiveData.observe(it, { aBoolean ->
                assertTrue(aBoolean)
            })
        }
    }

    @Test
    fun `Loading Changed On Start Bounding`() = mainCoroutineRule.run {
        // GIVEN
        val actual = true

        every { viewModel.bound(false) }

        // WHEN
        val expected = viewModel.usersLoading.value

        // THEN
        assertEquals(expected, actual)
    }

    @Test
    fun `Loading Not Changed On Start Bounding`() = mainCoroutineRule.run {
        // GIVEN
        val actual = false

        every { viewModel.bound(false) }

        // WHEN
        val expected = viewModel.usersLoading.value

        // THEN
        assertNotEquals(expected, actual)
    }

}