package com.example.hellofolk.base

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.animation.doOnEnd
import androidx.databinding.DataBindingUtil
import com.example.hellofolk.R
import com.example.hellofolk.databinding.ActivitySplashBinding


class SplashActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
        changeBackground("#FBAD18", "#5F4090", "#02D5B4")
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun changeBackground(color1: String?, color2: String?, color3: String?) {
        val anim = ValueAnimator()
        anim.setIntValues(
            Color.parseColor(color1),
            Color.parseColor(color2),
            Color.parseColor(color3)
        )
        anim.setEvaluator(ArgbEvaluator())
        anim.addUpdateListener { valueAnimator -> binding.container.setBackgroundColor(valueAnimator.animatedValue as Int) }
        anim.duration = 2000
        anim.start()
        anim.doOnEnd {
            binding.container.setBackgroundColor(Color.WHITE)
            binding.ivHelloFolk.setImageDrawable(getDrawable(R.drawable.ic_hello_folk_splash_colored))
            val t: Thread = object : Thread() {
                override fun run() {
                    try {
                        sleep(800)
                        startActivity(Intent(this@SplashActivity, WelcomeActivity::class.java))
                        finish()
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }
                }
            }
            t.start()
        }
    }
}