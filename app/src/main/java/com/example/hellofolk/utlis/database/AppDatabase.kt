package com.example.hellofolk.utlis.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.hellofolk.data.database.CommentDao
import com.example.hellofolk.data.database.PostDao
import com.example.hellofolk.data.database.UserDao
import com.example.hellofolk.domain.model.CommentData
import com.example.hellofolk.domain.model.PostData
import com.example.hellofolk.domain.model.UserData

@Database(entities = [PostData::class, UserData::class, CommentData::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun postsDao(): PostDao
    abstract fun usersDao(): UserDao
    abstract fun commentsDao(): CommentDao
}