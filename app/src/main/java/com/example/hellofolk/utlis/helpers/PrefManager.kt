package com.example.hellofolk.utlis.helpers

import android.content.Context
import android.content.SharedPreferences

class PrefManager(var _context: Context?) {


    var pref: SharedPreferences? = null
    var editor: SharedPreferences.Editor? = null

    // shared pref mode
    private var PRIVATE_MODE = 0

    // Shared preferences file name
    private val PREF_NAME = "hellofolk-welcome"

    private val IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch"

    init {
        pref = _context!!.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref?.edit()
    }


    fun setFirstTimeLaunch(isFirstTime: Boolean) {
        editor!!.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime)
        editor!!.commit()
    }

    fun isFirstTimeLaunch(): Boolean {
        return pref!!.getBoolean(IS_FIRST_TIME_LAUNCH, true)
    }

}