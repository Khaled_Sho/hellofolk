package com.test.cleanArchRoomTest.utils.database

import android.content.Context
import androidx.room.Room
import com.example.hellofolk.data.database.CommentDao
import com.example.hellofolk.data.database.PostDao
import com.example.hellofolk.data.database.UserDao
import com.example.hellofolk.utlis.database.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
class DatabaseModule {


    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "HelloFolk-DATA.db"
        ).allowMainThreadQueries()
            .build()
    }

    @Provides
    fun providePostsDao(database: AppDatabase): PostDao {
        return database.postsDao()
    }

    @Provides
    fun provideUsersDao(database: AppDatabase): UserDao {
        return database.usersDao()
    }

    @Provides
    fun provideCommentsDao(database: AppDatabase): CommentDao {
        return database.commentsDao()
    }


}