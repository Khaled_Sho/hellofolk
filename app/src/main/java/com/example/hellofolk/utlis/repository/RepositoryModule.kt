package com.example.hellofolk.utlis.repository


import com.example.hellofolk.data.repository.CommentsRepositoryImpl
import com.example.hellofolk.data.repository.PostsRepositoryImpl
import com.example.hellofolk.data.repository.UsersRepositoryImpl
import com.example.hellofolk.domain.repository.CommentsRepository
import com.example.hellofolk.domain.repository.PostsRepository
import com.example.hellofolk.domain.repository.UsersRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

@InstallIn(ApplicationComponent::class)
@Module
class RepositoryModule {
    @Provides
    fun providePostsRepository(repo: PostsRepositoryImpl): PostsRepository = repo

    @Provides
    fun provideUsersRepository(repo: UsersRepositoryImpl): UsersRepository = repo

    @Provides
    fun provideCommentsRepository(repo: CommentsRepositoryImpl): CommentsRepository = repo
}