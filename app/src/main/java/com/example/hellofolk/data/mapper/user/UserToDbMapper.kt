package com.example.hellofolk.data.mapper.user

import com.example.hellofolk.data.response.ResponseUser
import com.example.hellofolk.utlis.database.Mapper
import com.example.hellofolk.domain.model.UserData

class UserToDbMapper : Mapper<UserData, ResponseUser>() {
    override fun map(value: UserData?): ResponseUser {
        throw UnsupportedOperationException()
    }

    override fun reverseMap(value: ResponseUser?): UserData? {
        return if (value == null) {
            null
        } else {
            UserData(
                value.id,
                value.name
            )
        }
    }
}