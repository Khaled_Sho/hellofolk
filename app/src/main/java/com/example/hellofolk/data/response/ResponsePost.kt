package com.example.hellofolk.data.response

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponsePost(

    @field:JsonProperty("id")
    val id: Int? = null,

    @field:JsonProperty("userId")
    val userId: Int? = null,

    @field:JsonProperty("title")
    val title: String? = null,

    @field:JsonProperty("body")
    val body: String? = null,

    @field:JsonProperty("name")
    val name: String? = null

) : Parcelable
