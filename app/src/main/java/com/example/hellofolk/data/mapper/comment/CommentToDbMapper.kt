package com.example.hellofolk.data.mapper.comment

import com.example.hellofolk.data.response.ResponseComment
import com.example.hellofolk.domain.model.CommentData
import com.example.hellofolk.utlis.database.Mapper

class CommentToDbMapper : Mapper<CommentData, ResponseComment>() {
/*    override fun map(value: CommentData?): ResponseComment {
        throw UnsupportedOperationException()
    }*/

    override fun map(value: CommentData?): ResponseComment {
        return if (value == null) {
            throw UnsupportedOperationException()
        } else {
            ResponseComment(
                value.id,
                value.postId,
                value.name,
                value.body
            )
        }
    }

    override fun reverseMap(value: ResponseComment?): CommentData? {
        return if (value == null) {
            null
        } else {
            CommentData(
                value.id,
                value.postId,
                value.name,
                value.body
            )
        }
    }
}