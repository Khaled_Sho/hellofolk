package com.example.hellofolk.data.repository

import androidx.lifecycle.LiveData
import com.example.hellofolk.data.api.PostsApiImpl
import com.example.hellofolk.data.database.PostDao
import com.example.hellofolk.data.response.ResponsePost
import com.example.hellofolk.domain.model.PostData
import com.example.hellofolk.domain.model.PostUserData
import com.example.hellofolk.domain.repository.PostsRepository
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject


@Suppress("UNCHECKED_CAST")
class PostsRepositoryImpl @Inject constructor(
    private val postsApi: PostsApiImpl,
    private val postDao: PostDao?
) : PostsRepository {
    override fun getPostsFromDb(): Maybe<List<PostUserData>?> {
        return postDao!!.getAllPosts()
    }

    override fun getPosts(): Maybe<List<ResponsePost>> {
        return postsApi.getPosts()
    }

    override fun deleteAllPosts(): Single<Int> {
        return postDao!!.deleteAllPosts()
    }

    override fun deletePost(postData: PostData): Single<Int> {
        return postDao!!.deletePost(postData)
    }

    override fun updatePost(postData: PostData): Single<Int> {
        return postDao!!.updatePost(postData)
    }

    override fun getPostUser(id: Int): Maybe<List<Int>> {
        return postDao!!.getPostUser(id)
    }

    override fun getSpecificPost(id: Int): Single<ResponsePost> {
        return postsApi.getSpecificPost(id)
    }

    override fun getSpecificPostFromDB(id: Int): Maybe<PostData> {
        return postDao!!.getSpecificPost(id)
    }

    override fun insertAllPosts(data: List<PostData>): Maybe<List<Long>> {
        return postDao!!.insertPosts(data)
    }
}