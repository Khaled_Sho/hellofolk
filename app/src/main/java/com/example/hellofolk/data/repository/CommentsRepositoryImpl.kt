package com.example.hellofolk.data.repository

import com.example.hellofolk.data.api.CommentsApi
import com.example.hellofolk.data.database.CommentDao
import com.example.hellofolk.data.response.ResponseComment
import com.example.hellofolk.domain.model.CommentData
import com.example.hellofolk.domain.repository.CommentsRepository
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject


@Suppress("UNCHECKED_CAST")
class CommentsRepositoryImpl @Inject constructor(
    private val commentsApi: CommentsApi,
    private val commentDao: CommentDao
) : CommentsRepository {
    override fun getCommentsFromDb(): Maybe<List<CommentData>?> {
        return commentDao.getAllComments()
    }

    override fun getComments(): Maybe<List<ResponseComment>> {
        return commentsApi.getComments()
    }


    override fun deleteAllComments(): Single<Int> {
        return commentDao.deleteAllComments()
    }

    override fun deleteAllPostComments(postId: Int): Single<Int> {
        return commentDao.deleteAllPostComments(postId)
    }

    override fun getPostsCommentsFromDB(postId: Int): Maybe<List<CommentData>?> {
        return commentDao.getPostsComments(postId)
    }

    override fun getPostsComments(postId: Int): Maybe<List<ResponseComment>> {
        return commentsApi.getPostsComments(postId)
    }

    override fun getSpecificComment(id: Int): Single<ResponseComment> {
        return commentsApi.getSpecificComment(id)
    }

    override fun getSpecificCommentFromDB(id: Int): Maybe<CommentData> {
        return commentDao.getSpecificComment(id)
    }

    override fun insertAllComments(data: List<CommentData>): Maybe<List<Long>> {
        return commentDao.insertComments(data)
    }
}