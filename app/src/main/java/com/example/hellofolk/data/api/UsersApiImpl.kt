package com.example.hellofolk.data.api


import com.example.hellofolk.data.response.ResponseUser
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject

class UsersApiImpl @Inject constructor(private val apiInterface: UsersApi) {
    fun getUsers(): Maybe<List<ResponseUser>> {
        return apiInterface.getUsers()
    }
    fun getSpecificUser(id:Int):Single<ResponseUser>{
        return apiInterface.getSpecificUser(id)
    }
}