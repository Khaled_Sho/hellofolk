package com.example.hellofolk.data.repository

import com.example.hellofolk.data.api.UsersApiImpl
import com.example.hellofolk.data.database.UserDao
import com.example.hellofolk.data.response.ResponseUser
import com.example.hellofolk.domain.model.UserData
import com.example.hellofolk.domain.repository.UsersRepository
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject


@Suppress("UNCHECKED_CAST")
class UsersRepositoryImpl @Inject constructor(
    private val usersApi: UsersApiImpl,
    private val userDao: UserDao
) : UsersRepository {
    override fun getUsersFromDb(): Maybe<List<UserData>?> {
        return userDao.getAllUsers()
    }

    override fun getUsers(): Maybe<List<ResponseUser>> {
        return usersApi.getUsers()
    }

    override fun deleteAllUsers(): Single<Int> {
        return userDao.deleteAllUsers()
    }

    override fun getSpecificUser(id: Int): Single<ResponseUser> {
        return usersApi.getSpecificUser(id)
    }

    override fun getSpecificUserFromDB(id: Int): Maybe<UserData> {
        return userDao.getSpecificUser(id)
    }

    override fun insertAllUsers(data: List<UserData>): Maybe<List<Long>> {
        return userDao.insertUsers(data)
    }
}