package com.example.hellofolk.data.api

import com.example.hellofolk.data.response.ResponsePost
import io.reactivex.Maybe
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface PostsApi {
    @GET("posts")
    fun getPosts(): Maybe<List<ResponsePost>>

    @GET("posts/{id}")
    fun getSpecificPost(@Path("id") id: Int): Single<ResponsePost>

}