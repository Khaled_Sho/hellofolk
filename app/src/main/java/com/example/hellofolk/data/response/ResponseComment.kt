package com.example.hellofolk.data.response

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseComment(

    @field:JsonProperty("id")
    val id: Int? = null,

    @field:JsonProperty("postId")
    val postId: Int? = null,

    @field:JsonProperty("name")
    val name: String? = null,

    @field:JsonProperty("body")
    val body: String? = null

) : Parcelable
