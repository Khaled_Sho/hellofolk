package com.example.hellofolk.data.api

import com.example.hellofolk.data.response.ResponseUser
import io.reactivex.Maybe
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface UsersApi {
    @GET("users")
    fun getUsers(): Maybe<List<ResponseUser>>

    @GET("user/{id}")
    fun getSpecificUser(@Path("id") id: Int): Single<ResponseUser>

}