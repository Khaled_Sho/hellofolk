package com.example.hellofolk.data.api

import com.example.hellofolk.data.response.ResponsePost
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject

class PostsApiImpl @Inject constructor(private val apiInterface: PostsApi) {
    fun getPosts(): Maybe<List<ResponsePost>> {
        return apiInterface.getPosts()
    }
    fun getSpecificPost(id:Int):Single<ResponsePost>{
        return apiInterface.getSpecificPost(id)
    }
}