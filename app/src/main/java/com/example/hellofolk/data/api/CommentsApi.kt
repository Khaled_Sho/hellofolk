package com.example.hellofolk.data.api


import com.example.hellofolk.data.response.ResponseComment
import io.reactivex.Maybe
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CommentsApi {
    @GET("comments")
    fun getComments(): Maybe<List<ResponseComment>>

    @GET("comments/{id}")
    fun getSpecificComment(@Path("id") id: Int): Single<ResponseComment>

    @GET("comments")
    fun getPostsComments(@Query("postId") postId: Int): Maybe<List<ResponseComment>>

}