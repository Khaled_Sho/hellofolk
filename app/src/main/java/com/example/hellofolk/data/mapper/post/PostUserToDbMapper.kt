package com.example.hellofolk.data.mapper.post

import com.example.hellofolk.data.response.ResponsePost
import com.example.hellofolk.domain.model.PostUserData

import com.example.hellofolk.utlis.database.Mapper

class PostUserToDbMapper : Mapper<PostUserData, ResponsePost>() {
/*    override fun map(value: PostData?): ResponsePost {
        throw UnsupportedOperationException()
    }*/

    override fun map(value: PostUserData?): ResponsePost {
        return if (value == null) {
            throw UnsupportedOperationException()
        } else {
            ResponsePost(
                value.id,
                value.userId,
                value.title,
                value.body,
                value.name
            )
        }
    }

    override fun reverseMap(value: ResponsePost?): PostUserData? {
        return if (value == null) {
            null
        } else {
            PostUserData(
                value.id,
                value.userId,
                value.title,
                value.body,
                value.name
            )
        }
    }
}