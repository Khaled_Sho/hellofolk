package com.example.hellofolk.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.hellofolk.domain.model.UserData
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: UserData): Maybe<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUsers(userList: List<UserData>): Maybe<List<Long>>

    @Query("Select * From tbUser")
    fun getAllUsers(): Maybe<List<UserData>?>

    @Query("Select * From tbUser Where tbUser.id = :id")
    fun getSpecificUser(id: Int): Maybe<UserData>

    @Query("DELETE FROM tbUser")
    fun deleteAllUsers(): Single<Int>
}