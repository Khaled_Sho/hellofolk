package com.example.hellofolk.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.hellofolk.domain.model.PostData
import com.example.hellofolk.domain.model.PostUserData
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPost(post: PostData): Maybe<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPosts(postsList: List<PostData>): Maybe<List<Long>>

    @Query("Select tbPost.id, tbPost.body, tbPost.title, tbPost.userId, tbUser.name as name From tbPost left outer join tbUser on tbPost.userId == tbUser.id")
    fun getAllPosts(): Maybe<List<PostUserData>?>


    @Query("Select * From tbPost Where tbPost.id = :id")
    fun getSpecificPost(id: Int): Maybe<PostData>

    @Query("DELETE FROM tbPost")
    fun deleteAllPosts(): Single<Int>

    @Delete
    fun deletePost(post: PostData): Single<Int>

    @Update
    fun updatePost(post: PostData): Single<Int>

    @Query("Select userId From tbPost Where id = :id")
    fun getPostUser(id: Int): Maybe<List<Int>>
}