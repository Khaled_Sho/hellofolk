package com.example.hellofolk.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.hellofolk.domain.model.CommentData
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface CommentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertComment(comment: CommentData): Maybe<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertComments(commentsList: List<CommentData>): Maybe<List<Long>>

    @Query("Select * From tbComment")
    fun getAllComments(): Maybe<List<CommentData>?>

    @Query("Select * From tbComment Where tbComment.id = :id")
    fun getSpecificComment(id: Int): Maybe<CommentData>

    @Query("DELETE FROM tbComment")
    fun deleteAllComments(): Single<Int>

    @Query("DELETE FROM tbComment where postId = :postId")
    fun deleteAllPostComments(postId: Int): Single<Int>

    @Query("Select * From tbComment Where postId = :postId")
    fun getPostsComments(postId: Int): Maybe<List<CommentData>?>
}