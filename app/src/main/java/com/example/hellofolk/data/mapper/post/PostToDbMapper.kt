package com.example.hellofolk.data.mapper.post

import com.example.hellofolk.data.response.ResponsePost
import com.example.hellofolk.domain.model.PostData
import com.example.hellofolk.utlis.database.Mapper

class PostToDbMapper : Mapper<PostData, ResponsePost>() {
/*    override fun map(value: PostData?): ResponsePost {
        throw UnsupportedOperationException()
    }*/

    override fun map(value: PostData?): ResponsePost {
        return if (value == null) {
            throw UnsupportedOperationException()
        } else {
            ResponsePost(
                value.id,
                value.userId,
                value.title,
                value.body
            )
        }
    }

    override fun reverseMap(value: ResponsePost?): PostData? {
        return if (value == null) {
            null
        } else {
            PostData(
                value.id,
                value.userId,
                value.title,
                value.body
            )
        }
    }
}