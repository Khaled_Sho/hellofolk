package com.example.hellofolk.data.api

import com.example.hellofolk.data.response.ResponseComment
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject

class CommentsApiImpl @Inject constructor(private val apiInterface: CommentsApi) {
    fun getComments(): Maybe<List<ResponseComment>> {
        return apiInterface.getComments()
    }

    fun getSpecificComment(id: Int): Single<ResponseComment> {
        return apiInterface.getSpecificComment(id)
    }

    fun getPostsComments(postId: Int): Maybe<List<ResponseComment>> {
        return apiInterface.getPostsComments(postId)
    }

}