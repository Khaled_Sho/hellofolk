package com.example.hellofolk.ui.post

import com.example.hellofolk.domain.model.PostUserData


interface PostClicked {
    fun onPostClicked(postData: PostUserData)
}