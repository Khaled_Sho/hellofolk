package com.example.hellofolk.ui.comments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.hellofolk.R
import com.example.hellofolk.data.response.ResponsePost
import com.example.hellofolk.databinding.FragmentCommentsBinding
import com.example.hellofolk.domain.model.CommentData
import com.example.hellofolk.utlis.ext.addTo
import com.example.hellofolk.utlis.network.NetworkConnection
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.disposables.CompositeDisposable


@AndroidEntryPoint
class CommentsFragment : Fragment() {

    private val commentsViewModel: CommentsViewModel by viewModels()
    private lateinit var adapter: CommentsAdapter

    private var commentsList = ArrayList<CommentData>()

    private val disposables = CompositeDisposable()
    private lateinit var binding: FragmentCommentsBinding

    private val postData by lazy {
        arguments?.getParcelable<ResponsePost>("postData") as ResponsePost
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_comments, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initAdapter()
        postData.id?.let { commentsViewModel.bound(NetworkConnection.checkNetwork(), it) }
        observeData()
    }


    private fun initView() {
        initToolbar()
        initPost()
        binding.btnRetry.setOnClickListener {
            if (NetworkConnection.checkNetwork()) {
                binding.noInternetAnimationView.visibility = View.GONE
                binding.btnRetry.visibility = View.GONE
                postData.id?.let { commentsViewModel.bound(NetworkConnection.checkNetwork(), it) }
            }
        }
    }

    private fun initToolbar() {
        val navHostFragment = NavHostFragment.findNavController(this)
        NavigationUI.setupWithNavController(binding.toolbar, navHostFragment)
    }

    private fun initPost() {
        binding.tvTitle.text = postData.title
        binding.tvBody.text = postData.body
        binding.tvUserName.text = postData.name
    }

    private fun initAdapter() {
        adapter = CommentsAdapter(requireActivity(), commentsList)
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.commentsList.layoutManager = layoutManager
        binding.commentsList.adapter = adapter
    }

    private fun observeData() {

        commentsViewModel.commentsList.observe(viewLifecycleOwner, {
            commentsList.addAll(it)
            adapter.notifyDataSetChanged()
        })

        commentsViewModel.progressVisible.observe(viewLifecycleOwner, {
            if (it) {
                binding.commentsLoadingAnimationView.visibility = View.VISIBLE
                binding.commentsList.visibility = View.GONE
            } else {
                binding.commentsLoadingAnimationView.visibility = View.GONE
                binding.commentsList.visibility = View.VISIBLE
            }
        })
    }

    override fun onResume() {
        super.onResume()
        commentsViewModel.showErrorGettingComments.observe()
            .subscribe {
                if (it) {
                    binding.noInternetAnimationView.visibility = View.VISIBLE
                    binding.btnRetry.visibility = View.VISIBLE
                }
            }.addTo(disposables)
    }

    override fun onPause() {
        disposables.clear()
        super.onPause()
    }

    override fun onDestroy() {
        commentsViewModel.unbound()
        super.onDestroy()
    }
}