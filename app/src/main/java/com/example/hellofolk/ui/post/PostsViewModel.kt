package com.example.hellofolk.ui.post

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.example.hellofolk.domain.model.PostUserData
import com.example.hellofolk.domain.usecase.post.PostsUseCases
import com.example.hellofolk.utlis.helpers.SingleLiveEvent
import com.example.hellofolk.utlis.ext.addTo
import com.example.hellofolk.utlis.rx.StickyAction
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class PostsViewModel @ViewModelInject constructor(
    private val postsUseCases: PostsUseCases
) :
    ViewModel() {

    private val disposables = CompositeDisposable()
    val postsList = SingleLiveEvent<List<PostUserData>>()
    val showErrorGettingPosts = StickyAction<Boolean>()

    private val _progressVisible = SingleLiveEvent<Boolean>()
    val progressVisible: SingleLiveEvent<Boolean> = _progressVisible
    init {
        _progressVisible.value = true
    }

    fun bound(onlineData: Boolean) {
        if (!onlineData) {
            postsUseCases.getPostsFromDb()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleResult(it) }
                .addTo(disposables)
        } else {
            postsUseCases.insertFromApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleApiResult(it) }
                .addTo(disposables)
        }
    }

    private fun handleResult(result: PostsUseCases.Result) {
        when (result) {
            is PostsUseCases.Result.Loading -> progressVisible.value = true
            is PostsUseCases.Result.Success -> {
                val list: List<PostUserData> = result.responsePosts as List<PostUserData>
                if (list.isNotEmpty()) {
                    postsList.value = list
                } else {
                    showErrorGettingPosts.trigger(true)
                }
                progressVisible.value = false
            }
            is PostsUseCases.Result.Failure -> {
                showErrorGettingPosts.trigger(true)
                progressVisible.value = false
            }
        }
    }

    private fun handleApiResult(result: PostsUseCases.Result) {
        when (result) {
            is PostsUseCases.Result.Loading -> progressVisible.value = true
            is PostsUseCases.Result.Success -> {
                bound(false)
            }
            is PostsUseCases.Result.Failure -> {
                showErrorGettingPosts.trigger(true)
                progressVisible.value = false
            }
        }
    }


    fun unbound() {
        disposables.clear()
    }

    override fun onCleared() {
        super.onCleared()
    }

}