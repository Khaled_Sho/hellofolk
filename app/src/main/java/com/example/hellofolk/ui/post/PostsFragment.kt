package com.example.hellofolk.ui.post

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.hellofolk.R
import com.example.hellofolk.data.mapper.post.PostUserToDbMapper
import com.example.hellofolk.databinding.FragmentPostsBinding
import com.example.hellofolk.domain.model.PostUserData
import com.example.hellofolk.users.view.user.UsersViewModel
import com.example.hellofolk.utlis.ext.addTo
import com.example.hellofolk.utlis.network.NetworkConnection
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.disposables.CompositeDisposable


@AndroidEntryPoint
class PostsFragment : Fragment(), PostClicked {

    private val postsViewModel: PostsViewModel by viewModels()
    private val usersViewModel: UsersViewModel by viewModels()

    private lateinit var binding: FragmentPostsBinding
    private lateinit var adapter: PostsAdapter

    private var postsList = ArrayList<PostUserData>()

    private val disposables = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_posts, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initAdapter()
        usersViewModel.bound(NetworkConnection.checkNetwork())
        observeData()
    }

    private fun initView() {

        initToolbar()

        binding.btnRetry.setOnClickListener {
            if (NetworkConnection.checkNetwork()) {
                binding.noInternetAnimationView.visibility = View.GONE
                binding.btnRetry.visibility = View.GONE
                usersViewModel.bound(NetworkConnection.checkNetwork())
            }
        }
    }


    private fun initToolbar() {
        val radius = resources.getDimension(R.dimen.default_corner_radius)
        val materialShapeDrawable = binding.toolbar.background as MaterialShapeDrawable
        materialShapeDrawable.shapeAppearanceModel =
            materialShapeDrawable.shapeAppearanceModel.toBuilder()
                .setBottomLeftCorner(CornerFamily.ROUNDED, radius)
                .setBottomRightCorner(CornerFamily.ROUNDED, radius)
                .build()
        (activity as AppCompatActivity).setSupportActionBar(binding.toolbar)
        if ((activity as AppCompatActivity).supportActionBar != null) {
            (activity as AppCompatActivity).supportActionBar!!.setDisplayShowTitleEnabled(false)
        }
        val appBarConfiguration = AppBarConfiguration(setOf(R.id.PostsFragment))
        val navHostFragment = NavHostFragment.findNavController(this);
        NavigationUI.setupWithNavController(binding.toolbar, navHostFragment, appBarConfiguration)
    }

    private fun initAdapter() {
        adapter = PostsAdapter(requireActivity(), postsList, this)
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.postsList.layoutManager = layoutManager
        val itemDecorator = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        context?.let {
            ContextCompat.getDrawable(it, R.drawable.divider)?.let { itemDecorator.setDrawable(it) }
        }
        binding.postsList.addItemDecoration(itemDecorator)
        binding.postsList.adapter = adapter
    }

    private fun observeData() {
        usersViewModel.usersLoading.observe(viewLifecycleOwner, {
            if (it) {
                binding.loadingAnimationView.visibility = View.VISIBLE
                binding.postsList.visibility = View.GONE
                postsViewModel.bound(NetworkConnection.checkNetwork())
            }
        })

        postsViewModel.postsList.observe(viewLifecycleOwner, {
            postsList.addAll(it)
            adapter.notifyDataSetChanged()
        })

        postsViewModel.progressVisible.observe(viewLifecycleOwner, {
            if (it) {
                binding.loadingAnimationView.visibility = View.VISIBLE
                binding.postsList.visibility = View.GONE
            } else {
                binding.loadingAnimationView.visibility = View.GONE
                binding.postsList.visibility = View.VISIBLE
            }
        })
    }

    override fun onResume() {
        super.onResume()
        postsViewModel.showErrorGettingPosts.observe()
            .subscribe {
                if (it) {
                    binding.noInternetAnimationView.visibility = View.VISIBLE
                    binding.btnRetry.visibility = View.VISIBLE
                }
            }.addTo(disposables)
    }

    override fun onPause() {
        disposables.clear()
        super.onPause()
    }

    override fun onDestroy() {
        postsViewModel.unbound()
        super.onDestroy()
    }


    override fun onPostClicked(postData: PostUserData) {
        val list = ArrayList<PostUserData>()
        list.add(postData)
        val data = PostUserToDbMapper().map(list)
        //TODO open character episodes page
        val bundle = bundleOf("postData" to data[0])
        findNavController().navigate(R.id.action_navigation_postList_to_CommentsFragment, bundle)
    }
}

