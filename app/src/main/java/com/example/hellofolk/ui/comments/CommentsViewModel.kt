package com.example.hellofolk.ui.comments

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.example.hellofolk.domain.model.CommentData
import com.example.hellofolk.domain.usecase.comment.CommentsUseCases
import com.example.hellofolk.utlis.helpers.SingleLiveEvent
import com.example.hellofolk.utlis.ext.addTo
import com.example.hellofolk.utlis.rx.StickyAction
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class CommentsViewModel @ViewModelInject constructor(
    private val CommentsUseCases: CommentsUseCases
) :
    ViewModel() {

    private val disposables = CompositeDisposable()
    val commentsList = SingleLiveEvent<List<CommentData>>()
    val showErrorGettingComments = StickyAction<Boolean>()
    private var postId = -1;
    private val _progressVisible = SingleLiveEvent<Boolean>()
    val progressVisible: SingleLiveEvent<Boolean> = _progressVisible
    init {
        _progressVisible.value = true
    }

    fun bound(onlineData: Boolean, postId: Int) {
        this.postId = postId
        if (!onlineData) {
            CommentsUseCases.getPostsFromDb(postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleResult(it) }
                .addTo(disposables)
        } else {
            CommentsUseCases.insertFromApi(postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleApiResult(it) }
                .addTo(disposables)
        }
    }

    private fun handleResult(result: CommentsUseCases.Result) {
        when (result) {
            is CommentsUseCases.Result.Loading -> progressVisible.value = true
            is CommentsUseCases.Result.Success -> {
                val list: List<CommentData> = result.responseComments
                if (list.isNotEmpty()) {
                    commentsList.value = list
                } else {
                    showErrorGettingComments.trigger(true)
                }
                progressVisible.value = false
            }
            is CommentsUseCases.Result.Failure -> {
                showErrorGettingComments.trigger(true)
                progressVisible.value = false
            }
        }
    }

    private fun handleApiResult(result: CommentsUseCases.Result) {
        when (result) {
            is CommentsUseCases.Result.Loading -> progressVisible.value = true
            is CommentsUseCases.Result.Success -> {
                bound(false, postId)
            }
            is CommentsUseCases.Result.Failure -> {
                showErrorGettingComments.trigger(true)
                progressVisible.value = false
            }
        }
    }


    fun unbound() {
        disposables.clear()
    }

    override fun onCleared() {
        super.onCleared()
    }


}