package com.example.hellofolk.ui.post

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView

import com.example.hellofolk.R
import com.example.hellofolk.databinding.ItemPostsListBinding
import com.example.hellofolk.domain.model.PostUserData

class PostsAdapter(
    private var activity: Activity,
    var list: ArrayList<PostUserData>,
    private var onPostClicked: PostClicked
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var layoutInflater: LayoutInflater? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val binding: ItemPostsListBinding = DataBindingUtil.inflate(
            layoutInflater!!,
            R.layout.item_posts_list,
            parent,
            false
        )
        return PostsViewHolder(binding, activity)


    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is PostsViewHolder) {
            holder.setData(list[position])
            holder.binding.postLayout.setOnClickListener {
                onPostClicked.onPostClicked(list[position])
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class PostsViewHolder(
        var binding: ItemPostsListBinding,
        private val activity: Activity
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun setData(data: PostUserData) {
            binding.tvTitle.text = data.title
            binding.tvBody.text = data.body
            data.name?.let { binding.tvUserName.text = data.name }
            data.name = binding.tvUserName.text.toString()
        }
    }
}