package com.example.hellofolk.users.view.user

import android.annotation.SuppressLint
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.hellofolk.domain.model.UserData
import com.example.hellofolk.domain.usecase.user.GetUsersUseCase
import com.example.hellofolk.domain.usecase.user.InsertUserUseCase
import com.example.hellofolk.utlis.helpers.SingleLiveEvent
import com.example.hellofolk.utlis.ext.addTo
import com.example.hellofolk.utlis.rx.StickyAction
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class UsersViewModel @ViewModelInject constructor(
    private val getUsersUseCase: GetUsersUseCase,
    private val insertUserUseCase: InsertUserUseCase
) :
    ViewModel() {

    private val disposables = CompositeDisposable()
    val usersList = MutableLiveData<List<UserData>>()
    val showErrorGettingUsers = StickyAction<Boolean>()

    private val _usersLoading = SingleLiveEvent<Boolean>()
    val usersLoading: SingleLiveEvent<Boolean> = _usersLoading
    init {
        _usersLoading.value = true
    }

    fun bound(hasNetwork: Boolean) {
        getUsersUseCase.getUsers(hasNetwork)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { handleResult(it) }
            .addTo(disposables)

    }

    private fun handleResult(result: GetUsersUseCase.Result) {
        when (result) {
            is GetUsersUseCase.Result.Loading -> usersLoading.value = true
            is GetUsersUseCase.Result.Success -> {
                insert(result.responseUsers)
                usersList.value = result.responseUsers
                usersLoading.value = false
            }
            is GetUsersUseCase.Result.Failure -> {
                showErrorGettingUsers.trigger(true)
                usersLoading.value = false
            }
        }
    }

    @SuppressLint("CheckResult")
    private fun insert(data: List<UserData>) {
        insertUserUseCase.insertUsers(data)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun unbound() {
        disposables.clear()
    }

    override fun onCleared() {
        super.onCleared()

    }


}