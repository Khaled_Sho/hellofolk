package com.example.hellofolk.ui.comments

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.hellofolk.R
import com.example.hellofolk.databinding.ItemCommentsListBinding
import com.example.hellofolk.domain.model.CommentData

class CommentsAdapter(
    private var activity: Activity,
    var list: ArrayList<CommentData>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var layoutInflater: LayoutInflater? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val binding: ItemCommentsListBinding = DataBindingUtil.inflate(
            layoutInflater!!,
            R.layout.item_comments_list,
            parent,
            false
        )
        return CommentsViewHolder(binding, activity)


    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CommentsViewHolder) {
            holder.setData(list[position])
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class CommentsViewHolder(
        var binding: ItemCommentsListBinding,
        private val activity: Activity
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun setData(data: CommentData) {
            binding.tvName.text = data.name
            binding.tvBody.text = data.body
        }
    }
}