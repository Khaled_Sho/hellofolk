package com.example.hellofolk.domain.usecase.comment


import com.example.hellofolk.data.mapper.comment.CommentToDbMapper
import com.example.hellofolk.domain.model.CommentData
import com.example.hellofolk.domain.repository.CommentsRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CommentsUseCases @Inject constructor(private val commentsRepository: CommentsRepository) {

    sealed class Result {
        object Loading : Result()
        data class Success(val responseComments: List<CommentData>) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun getPostsFromDb(postId: Int): Observable<Result> {
        return commentsRepository.getPostsCommentsFromDB(postId)
            .toObservable()
            .map {
                Result.Success(it) as Result
            }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }


    fun insertFromApi(postId: Int): Observable<Result> {
        commentsRepository.deleteAllPostComments(postId)
        return commentsRepository.getPostsComments(postId).toObservable().map {
            val data = CommentToDbMapper().reverseMap(it)
            insert(data)
            Result.Success(data) as Result
        }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }

    private fun insert(data: List<CommentData>) {
        commentsRepository.insertAllComments(data).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

}