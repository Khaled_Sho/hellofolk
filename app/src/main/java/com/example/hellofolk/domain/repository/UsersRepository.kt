package com.example.hellofolk.domain.repository

import com.example.hellofolk.data.response.ResponseUser

import com.example.hellofolk.domain.model.UserData
import io.reactivex.Maybe
import io.reactivex.Single

interface UsersRepository {

    fun getUsersFromDb(): Maybe<List<UserData>?>
    fun getUsers(): Maybe<List<ResponseUser>>
    fun getSpecificUser(id: Int): Single<ResponseUser>
    fun getSpecificUserFromDB(id: Int): Maybe<UserData>
    fun insertAllUsers(data: List<UserData>):Maybe<List<Long>>
    fun deleteAllUsers():Single<Int>
}