package com.example.hellofolk.domain.usecase.post


import com.example.hellofolk.data.mapper.post.PostToDbMapper
import com.example.hellofolk.domain.model.PostData
import com.example.hellofolk.domain.model.PostUserData
import com.example.hellofolk.domain.repository.PostsRepository
import com.example.hellofolk.domain.usecase.post.PostsUseCases.Result.Failure
import com.example.hellofolk.domain.usecase.post.PostsUseCases.Result.Success
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PostsUseCases @Inject constructor(private val postsRepository: PostsRepository) {

    sealed class Result {
        object Loading : Result()
        data class Success(val responsePosts: List<PostUserData>) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun getPostsFromDb(): Observable<Result> {
        return postsRepository.getPostsFromDb()
            .toObservable()
            .map {
                Success(it) as Result
            }
            .onErrorReturn { Failure(it) }
            .startWith(Result.Loading)
    }


    fun insertFromApi(): Observable<Result> {
        postsRepository.deleteAllPosts()
        return postsRepository.getPosts().toObservable().map {
            val data = PostToDbMapper().reverseMap(it)
            val list: List<PostUserData> = data as List<PostUserData>
            insert(list)
            Success(list) as Result
        }
            .onErrorReturn {
                Failure(it)
            }
            .startWith(Result.Loading)
    }


    fun insert(data: List<PostData>) {
        postsRepository.insertAllPosts(data).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }


}



