package com.example.hellofolk.domain.usecase.user

import com.example.hellofolk.domain.model.UserData
import com.example.hellofolk.domain.repository.UsersRepository
import io.reactivex.Maybe
import javax.inject.Inject

class InsertUserUseCase @Inject constructor(private val usersRepository: UsersRepository) {
    fun insertUsers(data: List<UserData>):Maybe<List<Long>>{
        return usersRepository.insertAllUsers(data)
    }
}