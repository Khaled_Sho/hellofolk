package com.example.hellofolk.domain.repository

import com.example.hellofolk.data.response.ResponseComment
import com.example.hellofolk.domain.model.CommentData
import io.reactivex.Maybe
import io.reactivex.Single

interface CommentsRepository {

    fun getCommentsFromDb(): Maybe<List<CommentData>?>
    fun getComments(): Maybe<List<ResponseComment>>
    fun getSpecificComment(id: Int): Single<ResponseComment>
    fun getSpecificCommentFromDB(id: Int): Maybe<CommentData>
    fun insertAllComments(data: List<CommentData>):Maybe<List<Long>>
    fun deleteAllComments():Single<Int>
    fun deleteAllPostComments(postId: Int):Single<Int>
    fun getPostsComments(postId: Int): Maybe<List<ResponseComment>>
    fun getPostsCommentsFromDB(postId: Int): Maybe<List<CommentData>?>

}