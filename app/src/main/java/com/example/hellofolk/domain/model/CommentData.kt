package com.example.hellofolk.domain.model

import androidx.room.*
import com.example.hellofolk.utlis.database.converter.StringConverter

@Entity(tableName = "tbComment")
@TypeConverters(StringConverter::class)
data class CommentData(
    @PrimaryKey
    val id: Int? = null,
    val postId: Int? = null,
    val name: String? = null,
    val body: String? = null
)