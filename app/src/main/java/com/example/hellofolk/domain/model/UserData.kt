package com.example.hellofolk.domain.model

import androidx.room.*
import com.example.hellofolk.utlis.database.converter.StringConverter

@Entity(tableName = "tbUser")
@TypeConverters(StringConverter::class)
data class UserData(
    @PrimaryKey
    val id: Int? = null,
    val name: String? = null
)