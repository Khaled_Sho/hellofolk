package com.example.hellofolk.domain.usecase.user


import com.example.hellofolk.data.mapper.user.UserToDbMapper
import com.example.hellofolk.domain.model.UserData
import com.example.hellofolk.domain.repository.UsersRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetUsersUseCase @Inject constructor(private val usersRepository: UsersRepository) {

    sealed class Result {
        object Loading : Result()
        data class Success(val responseUsers: List<UserData>) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun getUsers(hasNetwork: Boolean): Observable<Result> {
        return if (!hasNetwork) {
            return usersRepository.getUsersFromDb()
                .toObservable()
                .map {
                    Result.Success(it) as Result
                }
                .onErrorReturn { Result.Failure(it) }
                .startWith(Result.Loading)
        } else {
            usersRepository.deleteAllUsers()
            usersRepository.getUsers().toObservable().map {
                val data = UserToDbMapper().reverseMap(it)
                Result.Success(data) as Result
            }
                .onErrorReturn { Result.Failure(it) }
                .startWith(Result.Loading)
        }
    }
}



