package com.example.hellofolk.domain.repository

import androidx.lifecycle.LiveData
import com.example.hellofolk.data.response.ResponsePost
import com.example.hellofolk.domain.model.PostData
import com.example.hellofolk.domain.model.PostUserData
import io.reactivex.Maybe
import io.reactivex.Single

interface PostsRepository {

    fun getPostsFromDb(): Maybe<List<PostUserData>?>
    fun getPosts(): Maybe<List<ResponsePost>>
    fun getSpecificPost(id: Int): Single<ResponsePost>
    fun getSpecificPostFromDB(id: Int): Maybe<PostData>
    fun insertAllPosts(data: List<PostData>):Maybe<List<Long>>
    fun deleteAllPosts():Single<Int>
    fun deletePost(postData: PostData):Single<Int>
    fun updatePost(postData: PostData):Single<Int>
    fun getPostUser(id: Int):Maybe<List<Int>>

}