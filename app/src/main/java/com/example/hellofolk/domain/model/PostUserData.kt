package com.example.hellofolk.domain.model

class PostUserData(
    id: Int?, userId: Int?, title: String?, body: String?, var name: String?
) : PostData(
    id,
    userId,
    title,
    body
)