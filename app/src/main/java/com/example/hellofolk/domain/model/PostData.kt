package com.example.hellofolk.domain.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.example.hellofolk.utlis.database.converter.StringConverter

@Entity(tableName = "tbPost")
@TypeConverters(StringConverter::class)
open class PostData(
    @PrimaryKey
    var id: Int? = null,
    var userId: Int? = null,
    var title: String? = null,
    var body: String? = null
)