package com.example.hellofolk.ui.post

import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.MediumTest
import com.example.hellofolk.R
import com.example.hellofolk.domain.model.PostUserData
import com.example.hellofolk.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

@MediumTest
@HiltAndroidTest
@ExperimentalCoroutinesApi
class PostsFragmentTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun setup() {
        hiltRule.inject()
    }

    @Test
    fun post_clicked_navigate_to_comment_fragment() {
        val navController: NavController = mock(NavController::class.java)

        launchFragmentInHiltContainer<PostsFragment> {
            Navigation.setViewNavController(requireView(), navController)
        }

        onView(withId(R.id.posts_list))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0,
                    click()
                )
            )

        val postUserData = PostUserData(1, 1, "Title", "Body", "Khaled")
        val bundle = bundleOf("postData" to postUserData)
        verify(
            navController.navigate(
                R.id.action_navigation_postList_to_CommentsFragment, bundle
            )
        )
    }

}