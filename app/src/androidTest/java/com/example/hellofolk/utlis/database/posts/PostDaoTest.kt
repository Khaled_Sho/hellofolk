package com.example.hellofolk.utlis.database.posts

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.hellofolk.data.database.PostDao
import com.example.hellofolk.domain.model.PostData
import com.example.hellofolk.utlis.database.AppDatabase
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject
import javax.inject.Named


@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
class PostDaoTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    @Named("test_db")
    lateinit var database: AppDatabase

    private lateinit var dao: PostDao


    @Before
    fun setup(){
        hiltRule.inject()
        dao = database.postsDao()
    }

    @Test
    fun insertPostTest() {
        val post = PostData(id = 1000, userId = 1, title = "Post Title", body = "Post Body")
        dao.insertPost(post).blockingGet()
        val postsSize = dao.getAllPosts().blockingGet()?.size
        assertEquals(postsSize, 1)
    }


    @Test
    fun insertPostsTest() {
        val post = PostData(id = 1000, userId = 1, title = "Post Title11", body = "Post Body11")
        val post2 = PostData(id = 1001, userId = 2, title = "Post Title22", body = "Post Body22")
        val postsList: MutableList<PostData> = mutableListOf()
        postsList.add(post)
        postsList.add(post2)
        dao.insertPosts(postsList).blockingGet()
        val postsSize = dao.getAllPosts().blockingGet()?.size
        assertEquals(postsSize , 2)
    }

    @Test
    fun getSpecificPost() {
        val post = PostData(id = 1000, userId = 1, title = "Post Title11", body = "Post Body11")
        val post2 = PostData(id = 1001, userId = 2, title = "Post Title22", body = "Post Body22")
        val postsList: MutableList<PostData> = mutableListOf()
        postsList.add(post)
        postsList.add(post2)
        dao.insertPosts(postsList).blockingGet()
        val postFromDb = dao.getSpecificPost(post.id!!).blockingGet()
        assertEquals(postFromDb.id , post.id)
    }

    @Test
    fun getPostUser() {
        val post = PostData(id = 1000, userId = 1, title = "Post Title11", body = "Post Body11")
        val post2 = PostData(id = 1001, userId = 2, title = "Post Title22", body = "Post Body22")
        val postsList: MutableList<PostData> = mutableListOf()
        postsList.add(post)
        postsList.add(post2)
        dao.insertPosts(postsList).blockingGet()
        val postFromDb = dao.getSpecificPost(post2.id!!).blockingGet()
        assertEquals(postFromDb.userId , 2)
    }


    @Test
    fun deletePostTest() {
        val post = PostData(id = 2, userId = 2, title = "Post Title 2", body = "Post Body 2")
        dao.insertPost(post).blockingGet()
        assertEquals(dao.getAllPosts().blockingGet()?.size, 1)
        dao.deletePost(post).blockingGet()
        assertEquals(dao.getAllPosts().blockingGet()?.size, 0)
    }

    @Test
    fun getPostAsLiveDataTest() {
        val post = PostData(id = 2, userId = 2, title = "Post Title 2", body = "Post Body 2")
        dao.insertPost(post).blockingGet()
        val postLiveDataValue = dao.getAllPosts().blockingGet()
        assertEquals(postLiveDataValue?.size, 1)
    }

    @Test
    fun updatePostTest() {
        val post = PostData(id = 2, userId = 2, title = "Post Title 2", body = "Post Body 2")
        dao.insertPost(post).blockingGet()
        post.userId = 3
        dao.updatePost(post).blockingGet()
        assertEquals(dao.getAllPosts().blockingGet()?.get(0)?.userId, 3)
    }
}