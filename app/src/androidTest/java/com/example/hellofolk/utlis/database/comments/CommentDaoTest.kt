package com.example.hellofolk.utlis.database.comments

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.hellofolk.data.database.CommentDao
import com.example.hellofolk.data.database.PostDao
import com.example.hellofolk.domain.model.CommentData
import com.example.hellofolk.utlis.database.AppDatabase
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject
import javax.inject.Named


@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
class CommentDaoTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    @Named("test_db")
    lateinit var database: AppDatabase

    private lateinit var dao: CommentDao


    @Before
    fun setup(){
        hiltRule.inject()
        dao = database.commentsDao()
    }
    
    
    @Test
    fun insertCommentTest() {
        val comment = CommentData(id = 1000, postId = 1, name = "Comment Name", body = "Comment Body")
        dao.insertComment(comment).blockingGet()
        val commentsSize = dao.getAllComments().blockingGet()?.size
        assertEquals(commentsSize, 1)
    }


    @Test
    fun insertCommentsTest() {
        val comment = CommentData(id = 1000, postId = 1, name = "Comment Name11", body = "Comment Body11")
        val comment2 = CommentData(id = 1001, postId = 2, name = "Comment Name22", body = "Comment Body22")
        val commentsList: MutableList<CommentData> = mutableListOf()
        commentsList.add(comment)
        commentsList.add(comment2)
        dao.insertComments(commentsList).blockingGet()
        val commentsSize = dao.getAllComments().blockingGet()?.size
        assertEquals(commentsSize , 2)
    }

    @Test
    fun getSpecificComment() {
        val comment = CommentData(id = 1000, postId = 1, name = "Comment Name11", body = "Comment Body11")
        val comment2 = CommentData(id = 1001, postId = 2, name = "Comment Name22", body = "Comment Body22")
        val commentsList: MutableList<CommentData> = mutableListOf()
        commentsList.add(comment)
        commentsList.add(comment2)
        dao.insertComments(commentsList).blockingGet()
        val commentFromDb = dao.getSpecificComment(comment.id!!).blockingGet()
        assertEquals(commentFromDb.id , comment.id)
    }

    @Test
    fun getPostsComments() {
        val comment = CommentData(id = 1000, postId = 1, name = "Comment Name11", body = "Comment Body11")
        val comment2 = CommentData(id = 1001, postId = 1, name = "Comment Name22", body = "Comment Body22")
        val commentsList: MutableList<CommentData> = mutableListOf()
        commentsList.add(comment)
        commentsList.add(comment2)
        dao.insertComments(commentsList).blockingGet()
        val commentsCount = dao.getPostsComments(1).blockingGet()?.size
        assertEquals(commentsCount , 2)
    }

    @Test
    fun deleteAllComments() {
        val comment = CommentData(id = 1000, postId = 1, name = "Comment Name11", body = "Comment Body11")
        val comment2 = CommentData(id = 1001, postId = 2, name = "Comment Name22", body = "Comment Body22")
        val commentsList: MutableList<CommentData> = mutableListOf()
        commentsList.add(comment)
        commentsList.add(comment2)
        dao.insertComments(commentsList).blockingGet()
        dao.deleteAllComments().blockingGet()
        val commentsCount = dao.getAllComments().blockingGet()?.size
        assertEquals(commentsCount , 0)
    }


    @Test
    fun deleteAllPostComments() {
        val comment = CommentData(id = 1000, postId = 1, name = "Comment Name11", body = "Comment Body11")
        val comment2 = CommentData(id = 1001, postId = 1, name = "Comment Name22", body = "Comment Body22")
        val comment3 = CommentData(id = 1002, postId = 2, name = "Comment Name33", body = "Comment Body33")
        val commentsList: MutableList<CommentData> = mutableListOf()
        commentsList.add(comment)
        commentsList.add(comment2)
        commentsList.add(comment3)
        dao.insertComments(commentsList).blockingGet()
        dao.deleteAllPostComments(1).blockingGet()
        val commentsCount = dao.getPostsComments(1).blockingGet()?.size
        assertEquals(commentsCount , 0)
    }
}