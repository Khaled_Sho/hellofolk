package com.example.hellofolk.utlis.database.users

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.hellofolk.data.database.UserDao
import com.example.hellofolk.domain.model.UserData
import com.example.hellofolk.utlis.database.AppDatabase
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject
import javax.inject.Named


@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
class UserDaoTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    @Named("test_db")
    lateinit var database: AppDatabase

    private lateinit var dao: UserDao


    @Before
    fun setup() {
        hiltRule.inject()
        dao = database.usersDao()
    }

    @Test
    fun insertUserTest() {
        val user = UserData(id = 1000, name = "Khaled Shoushara")
        dao.insertUser(user).blockingGet()
        val usersCount = dao.getAllUsers().blockingGet()?.size
        assertEquals(usersCount, 1)
    }


    @Test
    fun insertUsersTest() {
        val user = UserData(id = 1000, name = "Khaled Shoushara")
        val user2 = UserData(id = 1001, name = "Omar Shoushara")
        val usersList: MutableList<UserData> = mutableListOf()
        usersList.add(user)
        usersList.add(user2)
        dao.insertUsers(usersList).blockingGet()
        val usersCount = dao.getAllUsers().blockingGet()?.size
        assertEquals(usersCount, 2)
    }

    @Test
    fun getSpecificUser() {
        val user = UserData(id = 1000, name = "Khaled Shoushara")
        val user2 = UserData(id = 1001, name = "Omar Shoushara")
        val usersList: MutableList<UserData> = mutableListOf()
        usersList.add(user)
        usersList.add(user2)
        dao.insertUsers(usersList).blockingGet()
        val userFromDb = dao.getSpecificUser(user.id!!).blockingGet()
        assertEquals(userFromDb.id, user.id)
    }

    @Test
    fun getAllUsers() {
        val user = UserData(id = 1000, name = "Khaled Shoushara")
        val user2 = UserData(id = 1001, name = "Omar Shoushara")
        val user3 = UserData(id = 1002, name = "Ahmed Shoushara")
        val usersList: MutableList<UserData> = mutableListOf()
        usersList.add(user)
        usersList.add(user2)
        usersList.add(user3)
        dao.insertUsers(usersList).blockingGet()
        val usersCount = dao.getAllUsers().blockingGet()?.size
        assertEquals(usersCount, 3)
    }


    @Test
    fun deleteAllUsers() {
        val user = UserData(id = 1000, name = "Khaled Shoushara")
        val user2 = UserData(id = 1001, name = "Omar Shoushara")
        val user3 = UserData(id = 1002, name = "Ahmed Shoushara")
        val usersList: MutableList<UserData> = mutableListOf()
        usersList.add(user)
        usersList.add(user2)
        usersList.add(user3)
        dao.insertUsers(usersList).blockingGet()
        dao.deleteAllUsers().blockingGet()
        val usersCount = dao.getAllUsers().blockingGet()?.size
        assertEquals(usersCount, 0)
    }
}